import MySQLdb
import sys
import traceback
# fetch data from database
def get_data_by_query(host, user, passwd, db, _sql, is_update = 0):
    cur = conn = None
   # print "Query ::: " , _sql
    result = []
    try:
        conn = MySQLdb.connect(host = host, user = user, passwd = passwd, db = db)
        cur = conn.cursor()
        cur.execute(_sql)
        if is_update:
            conn.commit()
        result = cur.fetchall()
    except:
        print >> sys.stderr, "Error while query :: %s" %(_sql)
        print >> sys.stderr, traceback.format_exc()
        write_log("Error in query")
    finally:
        if cur:
            cur.close()
        if conn:
            conn.close()
 #   print result
    return result

#inset log in email_to_call.log file
def write_log(message):
	filename='auto_host.cfg'
	with open(filename, "w") as out:
		out.write(message)
		out.write('\n')


def start():
	query="SELECT * FROM auto_host"
	
	fetch_data =  get_data_by_query('127.0.0.1','root','cccl0g1c','nagios',query)
	host=''
	for value in fetch_data:
		host=host+'define host{\n'
		host=host+'\thost_name\t'+str(value[0])+'\n'
		host=host+'\tuse\t\t'+str(value[1])+'\n'
		host=host+'\thostgroups\t'+str(value[2])+'\n'
		host=host+'\talias\t\t'+str(value[3])+'\n'
		host=host+'\taddress\t\t'+str(value[4])+'\n'

		host=host+'\t}\n\n'
	write_log(host)
	

if __name__ == '__main__':
	start()

